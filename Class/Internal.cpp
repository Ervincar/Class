#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "Internal.h"

Internal::Internal(
	const char* manufacturer, 
	int capacity, 
	const char* type, 
	const char* name, 
	int frequency
) : Memory(manufacturer, capacity, type, name), frequency(frequency) {
	classname = T_INTERNAL;
}

Internal::Internal() : frequency(2600) {
	classname = T_INTERNAL;
}

void Internal::print() const {
	Memory::print();
	std::cout << ", type of connection - " << type_of_connection << ", memory frequency =" << frequency <<"GHz";
};


void Internal::save(std::ofstream& fout) {
	Memory::save(fout);
	fout.write(reinterpret_cast<char*>(type_of_connection), sizeof(type_of_connection));
	fout.write(reinterpret_cast<char*>(&frequency), sizeof(frequency));
}


void Internal::load(std::ifstream& fin) {
	Memory::load(fin);
	fin.read(reinterpret_cast<char*>(type_of_connection), sizeof(type_of_connection));
	fin.read(reinterpret_cast<char*>(&frequency), sizeof(frequency));
}

void Internal::input() {
		Memory::input();
    std::cout << "������� �������: ";
		while (true)
		{
				std::cin >> frequency;

				if (std::cin.fail())
				{
						std::cin.clear();
						std::cin.ignore(32767, '\n');
						std::cout << "�������� ����. ������� �������: ";
				}
				else {
						std::cin.ignore(32767, '\n');
						break;
				}
		}
    
}