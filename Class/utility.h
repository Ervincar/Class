#include "Memory.h"
#include "SSD.h"
#include "HDD.h"
#include "Operative.h"
#include "CMOS.h"
#include "List.h"


Memory* read_f(const char*, size_t&);
void smart_list_append(const char*, size_t&, List<Memory>&);