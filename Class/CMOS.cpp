#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "CMOS.h"

CMOS::CMOS(
	const char* manufacturer, 
	int capacity, 
	const char* type, 
	const char* name, 
	int frequency, 
	const char* type_of_BIOS
) : Internal(manufacturer, capacity, type, name, frequency){
	strcpy(this->type_of_BIOS, type_of_BIOS);
	classname = T_CMOS;
	is_nonvolatile = true;
}

CMOS::CMOS() {
	strcpy(type_of_BIOS, "UEFI");
	classname = T_CMOS;
	is_nonvolatile = true;
}

void CMOS::print() const {
	Internal::print();
	std::cout << ", is nonvolatile = " << is_nonvolatile << ", type of BIOS - " << type_of_BIOS << ")" << std::endl;
};


void CMOS::save(std::ofstream& fout) {
	Internal::save(fout);
	fout.write(reinterpret_cast<char*>(&is_nonvolatile), sizeof(is_nonvolatile));
	fout.write(reinterpret_cast<char*>(type_of_BIOS), sizeof(type_of_BIOS));
}


void CMOS::load(std::ifstream& fin) {
	Internal::load(fin);
	fin.read(reinterpret_cast<char*>(&is_nonvolatile), sizeof(is_nonvolatile));
	fin.read(reinterpret_cast<char*>(type_of_BIOS), sizeof(type_of_BIOS));
}

void CMOS::input() {
		Internal::input();
		std::cout << "������� ��� BIOS: ";
		while (true)
		{
				std::cin >> type_of_BIOS;

				if (std::cin.fail())
				{
						std::cin.clear();
						std::cin.ignore(32767, '\n');
						std::cout << "�������� ����. ������� ��� BIOS: ";
				}
				else {
						std::cin.ignore(32767, '\n');
						break;
				}
		}
}