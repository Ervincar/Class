#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "HDD.h"

HDD::HDD(const char* manufacturer, 
	int capacity, const char* type, 
	const char* name, 
	double form_factor, 
	int rotation_speed
) : External(manufacturer, capacity, type, name,form_factor), rotation_speed(rotation_speed){
    classname = T_HDD;
}

HDD::HDD() : rotation_speed(5200) {
    classname = T_HDD;
}

void HDD::print() const {
	External::print();
	std::cout << ", rotation speed = " << rotation_speed << ")" << std::endl;
}


void HDD::save(std::ofstream& fout) {
    External::save(fout);
    fout.write(reinterpret_cast<char*>(inf_carrier), sizeof(inf_carrier));
    fout.write(reinterpret_cast<char*>(&rotation_speed), sizeof(rotation_speed));
}


void HDD::load(std::ifstream& fin) {
    External::load(fin);
    fin.read(reinterpret_cast<char*>(inf_carrier), sizeof(inf_carrier));
    fin.read(reinterpret_cast<char*>(&rotation_speed), sizeof(rotation_speed));
}

void HDD::input() {
    External::input();
    std::cout << "������� �������� �������� ��������: ";
		while (true)
		{
				std::cin >> rotation_speed;

				if (std::cin.fail())
				{
						std::cin.clear();
						std::cin.ignore(32767, '\n');
						std::cout << "�������� ����. ������� �������� �������� ��������: ";
				}
				else {
						std::cin.ignore(32767, '\n');
						break;
				}
		}
}