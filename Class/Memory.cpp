#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <cstring>

#include "Memory.h"


Memory::Memory(const char* manufacturer, int capacity, const char* type, const char* name) : capacity(capacity) {
	strcpy_s(this->manufacturer, MAX_LENGTH, manufacturer);
	strcpy_s(this->type, MAX_LENGTH, type);
	strcpy_s(this->name, MAX_LENGTH, name);
}


Memory::Memory() : capacity(11) {
    strcpy(manufacturer, "Micron");
    classname = T_MEMORY;
    strcpy(type, "DDR2");
    strcpy(name, "HyperX Fury");
}

void Memory::set_capacity(int a){
    capacity = a;
}

const char* Memory::get_manufacturer() const{
	return manufacturer;
}

const char* Memory::get_type() const{
	return type;
}

const char* Memory::get_name() const{
    return name;
}

ClassName Memory::get_classname() const{
	return classname;
}


void Memory::print() const{
    std::cout 
        << this->classname << " " << this->name << " ("
        << "capacity = " << this->capacity
        << ", type = " << this->type << " "
        << ", manufacturer = " << this->manufacturer;
}

Memory::~Memory()
{
	//dtor
}


void Memory::save(std::ofstream& fout) {
    fout.write(reinterpret_cast<char*>(&classname), sizeof(classname));
    fout.write(reinterpret_cast<char*>(manufacturer), sizeof(manufacturer));
    fout.write(reinterpret_cast<char*>(&capacity), sizeof(capacity));
    fout.write(reinterpret_cast<char*>(type), sizeof(type));
    fout.write(reinterpret_cast<char*>(name), sizeof(name));
}


void Memory::load(std::ifstream& fin) {
    fin.read(reinterpret_cast<char*>(&classname), sizeof(classname));
    fin.read(reinterpret_cast<char*>(manufacturer), sizeof(manufacturer));
    fin.read(reinterpret_cast<char*>(&capacity), sizeof(capacity));
    fin.read(reinterpret_cast<char*>(type), sizeof(type));
    fin.read(reinterpret_cast<char*>(name), sizeof(name));
}

void Memory::input() {
    std::cout << "������� ��������: ";
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cin.getline(name, MAX_LENGTH);
    std::cout << "������� ��� ������: ";
    std::cin.getline(type, MAX_LENGTH);
    std::cout << "������� �������������: ";
    std::cin >> manufacturer;
    std::cout << "������� ������� ������: ";
    std::cin >> capacity;
}

std::ostream& operator<<(std::ostream& ostream, const Memory& memory) {
    memory.print();
    return ostream;
}