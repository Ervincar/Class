#include <fstream>
#include <iostream>

#include "utility.h"



Memory* read_f(const char* filename, size_t& offset) {
    ClassName classname;
    std::ifstream fin(filename, std::ios::binary | std::ios::app);
    fin.seekg(offset);
    fin.read(reinterpret_cast<char*>(&classname), sizeof(classname));
    fin.seekg(offset);

    Memory* memory;

    switch (classname) {
    case T_SSD:
        memory = new SSD;
        break;
    case T_HDD:
        memory = new HDD;
        break;
    case T_OPERATIVE:
        memory = new Operative;
        break;
    case T_CMOS:
        memory = new CMOS;
        break;
    default:
        return nullptr;
    }

    memory->load(fin);
    offset = fin.tellg();
    fin.close();

    return memory;
}

void smart_list_append(const char* filename, size_t& offset, List<Memory>& lst) {
    while (true) {
        Memory* memory = read_f(filename, offset);
        if (memory == nullptr) {
            break;
        }
        lst.append(memory);
    }
}