#pragma once

#include "Memory.h"
#include "config.h"

class Internal :
	public Memory
{
protected:
	char type_of_connection[MAX_LENGTH] = "Direct";
	int frequency;
public:
	Internal(const char*, int, const char*, const char*, int);
	Internal();

	virtual void print() const;

	void save(std::ofstream&);
	void load(std::ifstream&);
	void input();

};

