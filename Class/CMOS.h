#pragma once

#include "Internal.h"
#include "config.h"

class CMOS :
	public Internal
{
protected:
	bool is_nonvolatile;
	char type_of_BIOS[MAX_LENGTH];
public:
	CMOS(const char*, int, const char*, const char*, int, const char*);
	CMOS();

	virtual void print() const;

	void save(std::ofstream&);
	void load(std::ifstream&);
	void input();
};

