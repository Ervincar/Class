#pragma once

#include "Memory.h"
#include "config.h"


class External : public Memory
{
protected:
	char type_of_connection[MAX_LENGTH] = "SATA";
	double form_factor;
public:
	External(const char*, int, const char*, const char*, double);
	External();
	
	virtual void print() const;

	void save(std::ofstream&);
	void load(std::ifstream&);
	void input();
};

