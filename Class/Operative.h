#pragma once

#include "Internal.h"
#include "config.h"

class Operative :
	public Internal
{
protected:
	bool is_nonvolatile = false;
	int number_of_chips;
public:
	Operative(const char*, int, const char*, const char*, int, int);
	Operative();

	virtual void print() const;

	void save(std::ofstream&);
	void load(std::ifstream&);
	void input();
};

