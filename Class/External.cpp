#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "External.h"


External::External(
	const char* manufacturer, 
	int capacity, 
	const char* type, 
	const char* name, 
	double form_factor
) : Memory(manufacturer, capacity, type, name), form_factor(form_factor) {
	classname = T_EXTERNAL;
}

External::External() : form_factor(3.5) {
	classname = T_EXTERNAL;
}

void External::print() const {
	Memory::print();
	std::cout << ", type of connection - " << type_of_connection << ", formfactor - " << form_factor;
};


void External::save(std::ofstream& fout) {
	Memory::save(fout);
	fout.write(reinterpret_cast<char*>(type_of_connection), sizeof(type_of_connection));
	fout.write(reinterpret_cast<char*>(&form_factor), sizeof(form_factor));
}


void External::load(std::ifstream& fin) {
	Memory::load(fin);
	fin.read(reinterpret_cast<char*>(type_of_connection), sizeof(type_of_connection));
	fin.read(reinterpret_cast<char*>(&form_factor), sizeof(form_factor));
}

void External::input() {
		Memory::input();
		std::cout << "������� ����-������: ";
		while (true)
		{
				std::cin >> form_factor;

				if (std::cin.fail())
				{
						std::cin.clear();
						std::cin.ignore(32767, '\n'); 
						std::cout << "�������� ����. ������� ����-������: ";
				}
				else {
						std::cin.ignore(32767, '\n');
						break;
				}
		}
}