#pragma once
#include "External.h"
#include "config.h"

class SSD :
	public External
{
protected:
	char inf_carrier[MAX_LENGTH] = "Flash cells";
	int TBW_resource;
public:
	SSD(const char*, int, const char*, const char*, double, int);
	SSD();

	virtual void print() const;

	void save(std::ofstream&);
	void load(std::ifstream&);
	void input();
};

