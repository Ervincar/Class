#pragma once

const size_t MAX_LENGTH = 100;
enum ClassName {
    T_MEMORY,
    T_EXTERNAL,
    T_INTERNAL,
    T_SSD,
    T_HDD,
    T_OPERATIVE,
    T_CMOS,
};
