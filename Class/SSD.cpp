#define _CRT_SECURE_NO_WARNINGS

#include <fstream>
#include <iostream>
#include "SSD.h"


SSD::SSD(const char* manufacturer, 
	int capacity, 
	const char* type, 
	const char* name, 
	double form_factor, 
	int TBW_resource
) : External(manufacturer, capacity, type, name, form_factor), TBW_resource(TBW_resource) {
    classname = T_SSD;
}
SSD::SSD() : TBW_resource(10000) {
    classname = T_SSD;
}

void SSD::print() const {
	External::print();
	std::cout << ", TBW resource = " << TBW_resource << "GB" << ")" << std::endl;
}


void SSD::save(std::ofstream& fout) {
    External::save(fout);
    fout.write(reinterpret_cast<char*>(inf_carrier), sizeof(inf_carrier));
    fout.write(reinterpret_cast<char*>(&TBW_resource), sizeof(TBW_resource));
}


void SSD::load(std::ifstream& fin) {
    External::load(fin);
    fin.read(reinterpret_cast<char*>(inf_carrier), sizeof(inf_carrier));
    fin.read(reinterpret_cast<char*>(&TBW_resource), sizeof(TBW_resource));
}

void SSD::input() {
    External::input();
    std::cout << "������� ������ TBW: ";
		while (true)
		{
				std::cin >> TBW_resource;

				if (std::cin.fail())
				{
						std::cin.clear();
						std::cin.ignore(32767, '\n');
						std::cout << "�������� ����. ������� ������ TBW: ";
				}
				else {
						std::cin.ignore(32767, '\n');
						break;
				}
		}
}