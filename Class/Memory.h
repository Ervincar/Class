#pragma once

#include <cstdint>
#include <fstream>
#include <ostream>

#include "config.h"

class Memory
{
protected:
		ClassName classname;
private:
	char manufacturer[MAX_LENGTH] = "Samsung";
	int capacity;
	char type[MAX_LENGTH];
	char name[MAX_LENGTH];

public:
	Memory(const char*, int, const char*, const char*);
	Memory();
	
	virtual void print() const;
	virtual void save(std::ofstream&);
	virtual void load(std::ifstream&);
	virtual void input();
	const char* get_manufacturer() const;
	void set_capacity(int);
	const char* get_type() const;
	const char* get_name() const;
	ClassName get_classname() const;
	virtual ~Memory();

	friend std::ostream& operator<<(std::ostream&, const Memory&);

};

