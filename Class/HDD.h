#pragma once
#include "External.h"
#include "config.h"

class HDD :
	public External
{
protected:
	char inf_carrier[MAX_LENGTH] = "Disk";
	int rotation_speed;
public:
	HDD(const char*, int, const char*, const char*, double, int);
	HDD();

	virtual void print() const;

	void save(std::ofstream&);
	void load(std::ifstream&);
	void input();
};

