#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "Operative.h"

Operative::Operative(
	const char* manufacturer, 
	int capacity, 
	const char* type, 
	const char* name, 
	int frequency,
	int number_of_chips
) : Internal(manufacturer, capacity, type, name, frequency), number_of_chips(number_of_chips){
    classname = T_OPERATIVE;
	is_nonvolatile = false;
}

Operative::Operative() : number_of_chips(8) {
    classname = T_OPERATIVE;
	is_nonvolatile = false;
}

void Operative::print() const {
	Internal::print();
	std::cout << ", is nonvolatile = " << is_nonvolatile << ", number of memory chips =" << number_of_chips << ")" << std::endl;
};


void Operative::save(std::ofstream& fout) {
    Internal::save(fout);
    fout.write(reinterpret_cast<char*>(&is_nonvolatile), sizeof(is_nonvolatile));
    fout.write(reinterpret_cast<char*>(&number_of_chips), sizeof(number_of_chips));
}


void Operative::load(std::ifstream& fin) {
    Internal::load(fin);
    fin.read(reinterpret_cast<char*>(&is_nonvolatile), sizeof(is_nonvolatile));
    fin.read(reinterpret_cast<char*>(&number_of_chips), sizeof(number_of_chips));
}

void Operative::input() {
		Internal::input();
    std::cout << "������� ���������� ����� ������: ";
		while (true)
		{
				std::cin >> number_of_chips;

				if (std::cin.fail())
				{
						std::cin.clear();
						std::cin.ignore(32767, '\n');
						std::cout << "�������� ����. ������� ���������� ����� ������: ";
				}
				else {
						std::cin.ignore(32767, '\n');
						break;
				}
		}
}