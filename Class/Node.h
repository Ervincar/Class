#pragma once

template<typename T>
class Node{
private:
	T* data;
	Node<T> *next = nullptr;
public:
	Node(T* value) {
		this->data = value;
	}
    T* get_data() {
        return data;
    }

    Node<T>* get_next() {
        return next;
    }

    void set_next(Node<T>* next) {
        this->next = next;
    }

};