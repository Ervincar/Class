#include "Menu.h"

void menu_list() {
		setlocale(LC_ALL, "RU");
		int choose;
		int index;
		char name[MAX_LENGTH] = " ";
		size_t offset = 0;
		List<Memory> list;
		char filename[MAX_LENGTH];
		while (true) {
				std::cout 
						<< "����� ����������" << std::endl 
						<< "�������� ��������:" << std::endl
						<< "1. �������� ������ ��������" << std::endl 
						<< "2. ����� �������� � ������" << std::endl 
						<< "3. �������� �������� �� ������" << std::endl 
						<< "4. ���������� ������ � ����" << std::endl 
						<< "5. �������� ������ �� �����" << std::endl 
						<< "6. ������� ������" << std::endl 
						<< "7. �����" << std::endl;
				std::cin >> choose;
				switch (choose) {
				case 1:
						create_object(list);
						break;
				case 2:
						std::cout << "������� �������� ��� ������" << std::endl;
						std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
						std::cin.getline(name, MAX_LENGTH);
						std::cout << "��������� ������ �� �������:" << std::endl;
						list.search(name);
						system("pause");
						break;
				case 3:
						std::cout << "������� ���������� ����� �������� ��� ��������: " << std::endl;
						std::cin >> index;
						list.deleteAT(index);

						break;
				case 4:
						std::cout << "������� �������� ����� ��� ������:" << std::endl;
						std::cin >> filename;
						list.save_all(filename);
						break;
				case 5:
						std::cout << "������� �������� ����� ��� �����:" << std::endl;
						std::cin >> filename;
						smart_list_append(filename, offset, list);
						offset = 0;
						break;
				case 6:
						std::cout << "�������� ������ �� ������ ������:" << std::endl;
						list.print_all();
						system("pause");
						break;
				case 7:
						std::cout << "�������..." << std::endl;
						return;
				default:
						std::cout << "������� ������ ����� ����" << std::endl;
						system("pause");
				}
				system("cls");
		}
}

void create_object(List<Memory>& list){
		int classnameID;
		Memory* memory;
		std::cout << "������� ID ������" << std::endl << "1 - SSD" << std::endl << "2 - HDD" << std::endl << "3 - Operative" << std::endl << "4 - CMOS" << std::endl;
		std::cin >> classnameID;
		switch (classnameID) {
		case 1:
				memory = new SSD;
				memory->input();
				list.append(memory);
				break;
		case 2:
				memory = new HDD;
				memory->input();
				list.append(memory);
				break;
		case 3:
				memory = new Operative;
				memory->input();
				list.append(memory);
				break;
		case 4:
				memory = new CMOS;
				memory->input();
				list.append(memory);
				break;
		default:
				std::cout << "�������� ����! ������� � ����..." << std::endl;
		}
		system("pause");
		return;
}