#pragma once
#include <iostream>
#include <cstring>
#include "Node.h"

template<typename T>
class List
{
private:
	Node<T>* head;
	Node<T>* tail;
public:
	List() {
		head = nullptr;
		tail = nullptr;
	}

	Node<T>* get_head() {
		return head;
	}

	void append(T* data){
		Node<T>* node = new Node<T>(data);

		if (head == nullptr) {
			head = node;
			tail = head;
		} else {
			tail->set_next(node);
			tail = node;
		}
	}

	void print_all() {
			int n = 1;
		for (Node<T>* node = head; node != nullptr; node = node->get_next()) {
			T* memory = node->get_data();
			std::cout << n <<". ";
			std::cout << *memory;
			n++;
		}
	}

	void save_all(const char* filename) {
			std::ofstream fout(filename, std::ios::binary | std::ios::app);
			for (Node<T>* node = head; node != nullptr; node = node->get_next()) {
					T* memory = node->get_data();
					memory->save(fout);
			}
			fout.close();
	}

	void delete_front() {
		Node<T>* temp = head;

		head = head->get_next();

		delete temp;
	}

	void deleteAT(int index) {
		if (index == 1) {
			delete_front();
		}
		else{
			Node<T>* prev = this->head;
			for (int i = 0; i < index - 2; i++) {
					prev = prev->get_next();
			}
			Node<T>* toDel = prev->get_next();
			prev->set_next(toDel->get_next());
			
			delete toDel;
		}
	}

	void search(const char* name) {
			int n = 1;
			for (Node<T>* node = head; node != nullptr; node = node->get_next()) {
					T* memory = node->get_data();
					const char* temp = memory->get_name();
					if (std::strcmp(temp, name) == 0)
						std::cout << n << ". " << *memory;
					n++;
			}
	}

	~List() {
			while (head != nullptr) {
					delete_front();
			}
	}
};